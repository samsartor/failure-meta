use failure::{Error, Fail, AsFail, Backtrace};
use std::fmt;

#[derive(Debug)]
struct MetaFail<M> {
    pub cause: Error,
    pub meta: Option<M>,
}

impl<M> Fail for MetaFail<M>
    where M: fmt::Display + fmt::Debug + Sync + Send + 'static
{
    fn name(&self) -> Option<&str> {
        self.cause.name()
    }
    
    fn cause(&self) -> Option<&dyn Fail> {
        Some(self.cause.as_fail())
    }
    
    fn backtrace(&self) -> Option<&Backtrace> {
        Some(self.cause.backtrace())
    }
}

impl<M: fmt::Display> fmt::Display for MetaFail<M> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.meta {
            Some(m) => write!(f, "{} with {}", self.cause, m),
            None => write!(f, "{}", self.cause),
        }
    }
}

pub struct MetaError<M> {
    inner: MetaFail<M>
}

impl<M> MetaError<M> {
    pub fn new(cause: impl Into<Error>, meta: M) -> Self {
        MetaError::new_maybe(cause, Some(meta))
    }

    pub fn new_maybe(cause: impl Into<Error>, meta: Option<M>) -> Self {
        MetaError { inner: MetaFail {
            cause: cause.into(),
            meta,
        } }
    }

    pub fn top_meta(self) -> Option<M> {
        self.inner.meta
    }

    pub fn top_meta_ref(&self) -> Option<&M> {
        self.inner.meta.as_ref()
    }
    
    pub fn top_meta_mut(&mut self) -> Option<&mut M> {
        self.inner.meta.as_mut()
    }
}

impl<M> MetaError<M>
    where M: fmt::Display + fmt::Debug + Sync + Send + 'static
{
    pub fn to_error(self) -> Error {
        self.inner.into()
    }

    pub fn iter_meta<'a>(&'a self) -> impl Iterator<Item=&'a M> + 'a {
        self.as_fail().iter_chain().filter_map(|c| {
            match c.downcast_ref::<MetaFail<M>>() {
                Some(&MetaFail { meta: Some(ref m), .. }) => Some(m),
                _ => None,
            }
        })
    }

    pub fn find_meta(&self) -> Option<&M> {
        self.iter_meta().next()
    }
}

impl<M, E: Into<Error>> From<E> for MetaError<M>
    where M: fmt::Display + fmt::Debug + Sync + Send + 'static
{
    fn from(e: E) -> Self {
        match e.into().downcast::<MetaFail<M>>() {
            Ok(e) => MetaError { inner: e },
            Err(cause) => MetaError::new_maybe(cause, None),
        }
    }
}

impl<M> AsFail for MetaError<M>
    where M: fmt::Display + fmt::Debug + Sync + Send + 'static
{
    fn as_fail(&self) -> &dyn Fail {
        &self.inner
    }
}

impl<M: fmt::Display> fmt::Display for MetaError<M> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.inner, f)
    }
}

impl<M: fmt::Debug> fmt::Debug for MetaError<M> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, f)
    }
}

#[cfg(test)]
mod smalltests {
    use super::MetaError;
    use failure::{err_msg, Error};
    use std::fmt;

    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    struct A;
    impl fmt::Display for A {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "meta A")
        }
    }

    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    struct B;
    impl fmt::Display for B {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "meta B")
        }
    }

    #[test]
    fn to_error_and_back() {
        let a = A;
        let m: MetaError<A> = MetaError::new(err_msg("Foo"), a);
        let e: Error = m.to_error();
        let m: MetaError<A> = MetaError::from(e);
        assert_eq!(m.top_meta(), Some(a));
    }

    #[test]
    fn to_error_and_wrap() {
        let a = A;
        let m: MetaError<A> = MetaError::new(err_msg("Foo"), a);
        let e = m.to_error().context("Context");
        let m: MetaError<A> = MetaError::from(e);
        assert_eq!(m.top_meta(), None);
    }

    #[test]
    fn find_nested_metas() {
        let error = MetaError::new(
            err_msg("Foo"),
            B, // Find Some(B)
        );
        let error: MetaError<B> = MetaError::new_maybe(
            error.to_error().context("Context 2"),
            None, // Ignore None,
        );
        let error = MetaError::new(
            error.to_error(),
            A, // Ignore Some(A)
        );
        let error = MetaError::new(
            error.to_error().context("Context 1"),
            B, // Find Some(B)
        );
        assert_eq!(error.iter_meta().count(), 2);
    }

    #[test]
    fn passthrough_backtrace() {
        let e: Error = err_msg("Foo").into();
        let backtrace1 = format!("{}", e.backtrace());
        let e: Error = MetaError::<A>::from(e).to_error();
        let backtrace2 = format!("{}", e.backtrace());
        assert_eq!(backtrace1, backtrace2);
    }
}
